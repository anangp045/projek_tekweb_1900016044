import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MaterialDesign } from '../material';
import { ImagesComponent } from './images/images.component';
import { ForgotComponent } from './forgot/forgot.component';

const routes: Routes = [
  {
    path:'',
    component: AdminComponent,
    children:[
      {
        path:'dashboard',
        component:DashboardComponent
      },      
      {
        path:'',
        pathMatch:'full',
        redirectTo:'/admin/dashboard'
      }
    ]
  }  
]
@NgModule({
  declarations: [
    AdminComponent, 
    DashboardComponent,
    ImagesComponent,
    ForgotComponent    
  ],
  entryComponents:[
    
  ],
  imports: [
    CommonModule,   
    RouterModule.forChild(routes),   
    FormsModule,
    MaterialDesign,    
  ]
})
export class AdminModule { }